using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Kninect.Library.Echoplex;
using Microsoft.Kinect;

namespace ImagesTest
{
	public class Game1 : Microsoft.Xna.Framework.Game
	{
		private Player _echoPlexPlayer;

		readonly GraphicsDeviceManager graphics;
		SpriteBatch _spriteBatch;

		private KeyboardState PreviousState;

		private KinectSensor kinectSensor;
		private string KinectStatusString;
		Skeleton[] skeletons;

		private float RightHandX;
		private float RightHandY;

		//private Cell _echoPlexPlayer.PreviousCell;
		//private Cell currentCell;

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			graphics.PreferredBackBufferWidth = 1024;
			graphics.PreferredBackBufferHeight = 768;
			Content.RootDirectory = "Content";
		}

		protected override void Initialize()
		{
			_echoPlexPlayer = new Player(60, 60);

			KinectStatusString = "";

			try
			{
				//listen to any status change for Kinects
				KinectSensor.KinectSensors.StatusChanged += Kinects_StatusChanged;
				//loop through all the Kinects attached to this PC, and start the first that is connected without an error.
				foreach (KinectSensor kinect in KinectSensor.KinectSensors)
				{
					if (kinect.Status == KinectStatus.Connected)
					{
						kinectSensor = kinect;
						break;
					}
				}
				if (KinectSensor.KinectSensors.Count == 0)
					KinectStatusString ="No Kinect found";
				else
					InitializeKinect(); // Initialization of the current sensor
			}
			catch (Exception ex)
			{
				
			}
		


			base.Initialize();
		}

		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures, this is shared by everyone.  Should it?
			_spriteBatch = new SpriteBatch(GraphicsDevice);

			// Load everything for the echoplex player.  Send it the content and let it loads it's own resources
			_echoPlexPlayer.Load(this.Content);

			// Turn on Kinect
			KinectSensor.KinectSensors.StatusChanged += Kinects_StatusChanged;
		}

		protected override void UnloadContent()
		{
			CleanKinect();
			// TODO: Unload any non ContentManager content here
		}

		protected override void Update(GameTime gameTime)
		{
			// Get The keyboard
			var currentState = Keyboard.GetState();

			// Allows the game to exit
			if (currentState.IsKeyDown(Keys.Escape))
				Exit();

			// Update the Echoplex Player
			_echoPlexPlayer.Update(gameTime, currentState, PreviousState, GetFormatedXKinectPosition(), GetFormattedYKinectPostion());	

			// Set Previous keyboard state
			PreviousState = currentState;

			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			
			Vector2 kinectTextVector = new Vector2(20, 700);
			GraphicsDevice.Clear(Color.Black);
			_spriteBatch.Begin();

			_echoPlexPlayer.Draw(_spriteBatch, GetFormatedXKinectPosition(), GetFormattedYKinectPostion());
			_spriteBatch.DrawString(_echoPlexPlayer.Font, KinectStatusString, kinectTextVector, Color.Red);
			_spriteBatch.Draw(_echoPlexPlayer.KiniectPlaceholder, new Rectangle(700, 500,320, 240),Color.White);
			_spriteBatch.End();

			base.Draw(gameTime);
		}

		private int GetFormattedYKinectPostion()
		{
			float accl = (float)1.20;
			return (int)((768 * RightHandY) * accl)-200;
		}

		private int GetFormatedXKinectPosition()
		{
			float accl = (float)1.20;
			return (int)((1024 * RightHandX) * accl)-200;
		}

		void Kinects_StatusChanged(object sender, StatusChangedEventArgs e)
		{
			switch (e.Status)
			{
				case KinectStatus.Connected:
					if (kinectSensor == null)
					{
						kinectSensor = e.Sensor;
						InitializeKinect();
					}
					break;
				case KinectStatus.Disconnected:
					if (kinectSensor == e.Sensor)
					{
						CleanKinect();
						KinectStatusString = "Kinect was disconnected";
					}
					break;
				case KinectStatus.NotReady:
					break;
				case KinectStatus.NotPowered:
					if (kinectSensor == e.Sensor)
					{
						CleanKinect();
						KinectStatusString = "Kinect is no longer powered";
					}
					break;
				default:
					KinectStatusString = "Unhandled Status: " + e.Status;
					break;
			}
		}

		private void InitializeKinect()
		{
			if (kinectSensor == null)
				return;

			KinectStatusString = "starting kinect";
			
			kinectSensor.ColorStream.Enable();
			kinectSensor.SkeletonStream.Enable();
			kinectSensor.ColorFrameReady += new EventHandler<ColorImageFrameReadyEventArgs>(kinect_ColorFrameReady);
			kinectSensor.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(kinectSensor_SkeletonFrameReady);


			kinectSensor.Start();
			kinectSensor.ElevationAngle = 10;
		}

		private void kinectSensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
		{
			SkeletonFrame frame = e.OpenSkeletonFrame();

			if (frame == null)
				return;

			Skeleton[] skeletons = frame.GetSkeletons();

			if (skeletons.All(s => s.TrackingState == SkeletonTrackingState.NotTracked))
				return;

			foreach (Skeleton skeleton in skeletons)
			{
				if (skeleton.TrackingState != SkeletonTrackingState.Tracked)
					continue;

				var rightHand = skeleton.Joints.First(x => x.JointType == JointType.HandRight);
				Vector2 handVector = KinectTools.Convert(kinectSensor, rightHand.Position);

				RightHandX = handVector.X;
				RightHandY = handVector.Y;


			}


		}

		private void kinect_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs colorImageFrame)
		{
			 // Open image
			ColorImageFrame colorVideoFrame = colorImageFrame.OpenColorImageFrame();

			if (colorVideoFrame != null)
			{
				//Create array for pixel data and copy it from the image frame
				Byte[] pixelData = new Byte[colorVideoFrame.PixelDataLength];
				colorVideoFrame.CopyPixelDataTo(pixelData);

				_echoPlexPlayer.KiniectPlaceholder = new Texture2D(graphics.GraphicsDevice, colorVideoFrame.Width, colorVideoFrame.Height);
				_echoPlexPlayer.KiniectPlaceholder.SetData(pixelData);

			}



		}

		private void CleanKinect()
		{
			if (kinectSensor != null)
			{
				kinectSensor.Stop();
				kinectSensor = null;
			}
		}
	}

}
