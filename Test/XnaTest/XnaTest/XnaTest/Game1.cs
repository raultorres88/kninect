using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XnaTest
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class Game1 : Microsoft.Xna.Framework.Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		public int r, g, b;
		public bool rGoUp, bGoUp, gGoUp;

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			r = 0;
			g = 0;
			b = 0;
			rGoUp = bGoUp = gGoUp = true;

			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			// TODO: use this.Content to load your game content here
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			// Allows the game to exit
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				this.Exit();

			var pad = Keyboard.GetState();

			if (pad.IsKeyDown(Keys.R))
			{
				if (rGoUp)
				{
					r++;
				}
				else
				{
					r--;
				}

				if (r > 256)
					rGoUp = false;
				else if (r < 0)
					rGoUp = true;
			}


			if (pad.IsKeyDown(Keys.G))
			{
				if (gGoUp)
				{
					g++;
				}
				else
				{
					g--;
				}

				if (g > 256)
					gGoUp = false;
				else if (g < 0)
					gGoUp = true;
			}

			if (pad.IsKeyDown(Keys.B))
			{
				if (bGoUp)
				{
					b++;
				}
				else
				{
					b--;
				}

				if (b > 256)
					bGoUp = false;
				else if (b < 0)
					bGoUp = true;
			}


			base.Update(gameTime);
		}

		public void ColorUpdater(int value, bool direction)
		{
			
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			Color backgroundColor = new Color(r,g,b);
			GraphicsDevice.Clear(backgroundColor);
			base.Draw(gameTime);
		}
	}
}
