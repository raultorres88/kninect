using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Text;

namespace Kninect.Library.Echoplex
{
	public class Player
	{
		// Player Resources Textures
		public Texture2D ButtonBaseTexture { get; set; }
		public Texture2D ButtonPlayingTexture { get; set; }
		public Texture2D ButtonBeatTexture { get; set; }
		public Texture2D KiniectPlaceholder { get; set; }
		public Texture2D MousePointer { get; set; }
		public Texture2D MousePointer1 { get; set; }
		public Texture2D MousePointer2 { get; set; }
		public Texture2D MousePointer3 { get; set; }

		// Player sounds
		public SoundEffect Drum { get; set; }
		public SoundEffect Snare { get; set; }

		// Player Fonts
		public SpriteFont Font { get; set; }

		// Timer Data
		public bool PlaySound;
		public double NextBeat;
		public double BeatRate = 100;
		public double NextToggle;
		public double ToggleRate = 2000;
		public double TimeTillToggle = 0;

		// Keyboard UI Data
		public int KeyboardXValueInput = 0;
		public int KeyboardYValueInput = 0;
		public int KeyboardXValue = 0;
		public int KeyboardYValue = 0;

		// Kinect UI Data
		public Cell CurrentCell { get; set; }
		public Cell PreviousCell { get; set; }
		public int KinectXValue { get; set; }
		public int KinectYValue { get; set; }

		// Player Data
		public List<Cell> Cells;
		public int Beats = 32;
		public int Measures = 2;
		public int CurrentBeat = -1;
		public int Rows = 4;
		public int Columns = 16;

		public enum SoundType
		{
			Drum, Snare
		};

		public Player(int height, int width)
		{
			Cells = new List<Cell>(Rows * Columns);
			CurrentBeat = 0;
			NextToggle = 0;
			PlaySound = true;

			for (int row = 0; row < Rows; row++)
			{
				for (int column = 0; column < Columns; column++)
				{
					var cell = new Cell
					{
						DisplayRow = row,
						DisplayColumn = column,
						SequencerRow = row <= 1 ? 0 : 1,
						SequencerColumn = (row % 2) * 16 + column,
						Sound = row <= 1 ? SoundType.Drum : SoundType.Snare,
						Height = height,
						Width = width
					};

					cell.Init();
					Cells.Add(cell);

				}
			}
		}

		public void Beat()
		{
			CurrentBeat++;

			if (CurrentBeat >= Beats)
			{
				CurrentBeat = 0;
			}

			TurnOffPlaying();

			if (CurrentBeat >= 0)
			{
				GetCellBySequencerPosition(0, CurrentBeat).Playing = true;
				GetCellBySequencerPosition(1, CurrentBeat).Playing = true;
			}

		}

		public Cell GetSnareCell()
		{
			return Cells.First(x => x.SequencerColumn == CurrentBeat && x.Sound == SoundType.Snare);
		}

		public Cell GetDrumCell()
		{
			return Cells.First(x => x.SequencerColumn == CurrentBeat && x.Sound == SoundType.Drum);
		}

		public Cell GetCellByDisplayPosition(int row, int column)
		{
			return Cells.First(x => x.DisplayRow == row && x.DisplayColumn == column);
		}

		public Cell GetCellBySequencerPosition(int row, int column)
		{
			return Cells.First(x => x.SequencerRow == row && x.SequencerColumn == column);
		}

		public void TurnOffPlaying()
		{
			foreach (var cell in Cells)
			{
				cell.Playing = false;
			}
		}

		public Cell GetByRelativePosition(int x, int y)
		{
			foreach (var cell in Cells)
			{
				if (cell.GetRectangle().Left <= x && cell.GetRectangle().Left + cell.Width >= x)
				{
					if (cell.GetRectangle().Top <= y && cell.GetRectangle().Top + cell.Width >= y)
					{
						return cell;
					}
				}
			}

			return null;
		}

		public void Load(ContentManager content)
		{
			// Load textures into EchoPlexPlayer
			ButtonBaseTexture = content.Load<Texture2D>("base");
			ButtonPlayingTexture = content.Load<Texture2D>("playing");
			ButtonBeatTexture = content.Load<Texture2D>("beat");
			KiniectPlaceholder = content.Load<Texture2D>("KinectPlaceHolder");

			// MousePointers
			MousePointer = content.Load<Texture2D>("pointer");
			MousePointer1 = content.Load<Texture2D>("pointer_01");
			MousePointer2 = content.Load<Texture2D>("pointer_02");
			MousePointer3 = content.Load<Texture2D>("pointer_03");

			// Load Sounds
			Drum = content.Load<SoundEffect>("Drum");
			Snare = content.Load<SoundEffect>("Snare");

			// Load Font
			Font = content.Load<SpriteFont>("SpriteFont1");
			PlaySound = true;
		}

		public void GameTimeTick(GameTime gameTime)
		{
			// If enough time to do the next beat has passed
			if (gameTime.TotalGameTime.TotalMilliseconds >= NextBeat)
			{
				PlaySound = true;
				NextBeat = gameTime.TotalGameTime.TotalMilliseconds + BeatRate;
			}
		}

		public void SetCurrentKinectCell(int getFormatedXKinectPosition, int getFormattedYKinectPostion)
		{
			CurrentCell = GetByRelativePosition(getFormatedXKinectPosition, getFormattedYKinectPostion);
		}

		public void CheckCellToToggle(GameTime gameTime)
		{
			TimeTillToggle = 0;

			// If we have a current cell highlighted
			if (CurrentCell != null)
			{
				// If there has not been a previous cell highlighted, set it
				if (PreviousCell == null)
				{
					PreviousCell = CurrentCell;

				}

				// If we have kept the cell highlighted
				if (PreviousCell == CurrentCell)
				{
					// If this a toggle time has been set check it and switch if needed
					if (!NextToggle.Equals(0) && gameTime.TotalGameTime.TotalMilliseconds >= NextToggle)
					{
						CurrentCell.Toggle();
						PreviousCell = null;
						TimeTillToggle = 0;
					}

					// If no toggle time has been set yet, set it
					if (NextToggle.Equals(0))
					{
						NextToggle = gameTime.TotalGameTime.TotalMilliseconds + ToggleRate;
						TimeTillToggle = 0;
					}
					else
					{
						// up date the time to toggle if it's already been set
						TimeTillToggle = NextToggle - gameTime.TotalGameTime.TotalMilliseconds;
					}
				}

				if (PreviousCell != CurrentCell)
				{
					PreviousCell = CurrentCell;
					NextToggle = 0;
					TimeTillToggle = 0;
				}
			}
			else
			{
				TimeTillToggle = 0;
			}
		}

		public void KeyboardUpdates(KeyboardState currentState, KeyboardState previousState)
		{
			// Keyboard position data
			if (currentState.IsKeyDown(Keys.Up) && !previousState.IsKeyDown(Keys.Up))
				KeyboardXValueInput--;
			if (currentState.IsKeyDown(Keys.Down) && !previousState.IsKeyDown(Keys.Down))
				KeyboardXValueInput++;

			if (currentState.IsKeyDown(Keys.Right) && !previousState.IsKeyDown(Keys.Right))
				KeyboardYValueInput++;
			if (currentState.IsKeyDown(Keys.Left) && !previousState.IsKeyDown(Keys.Left))
				KeyboardYValueInput--;

			// Echoplex beat rate data
			if (currentState.IsKeyDown(Keys.Q) && !previousState.IsKeyDown(Keys.Up))
				BeatRate += 10;
			if (currentState.IsKeyDown(Keys.A) && !previousState.IsKeyDown(Keys.Down))
				BeatRate -= 10;

			// Range checks
			if (KeyboardYValueInput < 0)
				KeyboardYValueInput = 0;
			if (KeyboardXValueInput < 0)
				KeyboardXValueInput = 0;

			if (KeyboardYValueInput > 15)
				KeyboardYValueInput = 15;
			if (KeyboardXValueInput > 3)
				KeyboardXValueInput = 3;

			// Set actual X and Y value
			KeyboardXValue = KeyboardXValueInput;
			KeyboardYValue = KeyboardYValueInput;

			// Toggle if the spacebar is used
			if (currentState.IsKeyDown(Keys.Space) && !previousState.IsKeyDown(Keys.Space))
				GetCellByDisplayPosition(KeyboardXValue, KeyboardYValue).Toggle();
		}

		public void PlayBeat()
		{
			if (PlaySound)
			{
				Beat();
				if (GetDrumCell().On)
					Drum.Play();

				if (GetSnareCell().On)
					Snare.Play();

				PlaySound = false;
			}
		}

		public void Update(GameTime gameTime, KeyboardState currentState, KeyboardState previousState, int KinectX, int KinectY)
		{
			GameTimeTick(gameTime);

			SetCurrentKinectCell(KinectX, KinectY);

			CheckCellToToggle(gameTime);

			KeyboardUpdates(currentState, previousState);

			PlayBeat();

		}

		public void Draw(SpriteBatch spriteBatch, int xKinectPosition, int yKinectPosition)
		{
			KinectXValue = xKinectPosition;
			KinectYValue = yKinectPosition;

			// Draw All Cells that are off and not playing
			foreach (var cell in Cells.Where(x => !x.On && !x.Playing))
			{
				spriteBatch.Draw(ButtonBaseTexture, cell.GetRectangle(), Color.White);
			}

			// Draw all cells that are on and not playing
			foreach (var cell in Cells.Where(x => x.On && !x.Playing))
			{
				spriteBatch.Draw(ButtonPlayingTexture, cell.GetRectangle(), Color.White);
			}

			// Draw all cells that are playing
			foreach (var cell in Cells.Where(x => x.Playing))
			{
				spriteBatch.Draw(ButtonBeatTexture, cell.GetRectangle(), Color.White);
			}

			// Draw "MousePointer"
			spriteBatch.Draw(GetMousePointer(), new Rectangle(xKinectPosition, yKinectPosition, 50, 50), Color.White);

			// Diagnostic Data
			Vector2 textVector = new Vector2(20, 500);

			// Diagnostics
			spriteBatch.DrawString(Font, Diagnostics(), textVector, Color.Red);

		}

		private Texture2D GetMousePointer()
		{

			if (TimeTillToggle <= 0)
				return MousePointer;
			if (TimeTillToggle < 500.00)
				return MousePointer3;
			if (TimeTillToggle <= 1000.00)
				return MousePointer2;
			if (TimeTillToggle <= 1500.00)
				return MousePointer1;

			return MousePointer;
		}

		public string Diagnostics()
		{
			StringBuilder DiagntosticData = new StringBuilder();

			DiagntosticData.AppendFormat("Beat {0} of {1} at {2} Beat Rate \nHand Position {3} , {4}  \n", CurrentBeat, Beats, BeatRate.ToString(), KinectXValue, KinectYValue);

			if (CurrentCell != null)
				DiagntosticData.Append("The Current Cells position is {0} {1}\n", CurrentCell.DisplayRow, CurrentCell.DisplayColumn);
			else
				DiagntosticData.Append("No Current cell position\n");

			if (PreviousCell != null)
				DiagntosticData.Append("The Previous Cells position is {0} {1}\n", PreviousCell.DisplayRow, PreviousCell.DisplayColumn);
			else
				DiagntosticData.Append("No Previous cell position\n");

			DiagntosticData.Append(string.Format("\nThe next Toggle is in {0}", TimeTillToggle));

			return DiagntosticData.ToString();
		}
	}
}