﻿using System;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Kninect.Library.Echoplex
{
	public class Cell
	{
		//Image Details
		public int Width;
		public int Height;

		// Status
		public bool On;
		public bool Playing;
		
		// Screen Position
		public int ScreenX;
		public int ScreenY;
		public int TopMargin;
		private Rectangle RectanglePosition;

		// Row Position
		public int SequencerRow;
		public int SequencerColumn;
		public int DisplayRow;
		public int DisplayColumn;

		// Track Data
		public int Track
		{
			get { return SequencerRow; }
		}
		public int Beat
		{
			get { return SequencerColumn; }
		}

		public Player.SoundType Sound;

		public void Init()
		{
			RectanglePosition = new Rectangle(DisplayColumn * Height, DisplayRow * Width, Height, Width);
		}

		public Rectangle GetRectangle()
		{
		
			return RectanglePosition;
		}

		public void Toggle()
		{
			On = !On;
		}
	}
}
