﻿using NUnit.Framework;
using Kninect.Library.Echoplex;
using System.Linq;

namespace Kniect.Tests
{
	[TestFixture]
	class EchoplexTests
	{

		[Test]
		public void CellSetup()
		{
			var player = new Player(60, 60);

			// 64 cells set up
			Assert.AreEqual(64, player.Cells.Count);

			// 16 display cells per row
			Assert.AreEqual(16, player.Cells.Count(x => x.DisplayRow == 0), "Bad Row count for row 0");
			Assert.AreEqual(16, player.Cells.Count(x => x.DisplayRow == 1), "Bad Row count for row 1");
			Assert.AreEqual(16, player.Cells.Count(x => x.DisplayRow == 2), "Bad Row count for row 2");
			Assert.AreEqual(16, player.Cells.Count(x => x.DisplayRow == 3), "Bad Row count for row 3");

			// 32 cells per sequencer row
			Assert.AreEqual(32, player.Cells.Count(x => x.SequencerRow == 0), "Bad Row count for sequencer 0");
			Assert.AreEqual(32, player.Cells.Count(x => x.SequencerRow == 1), "Bad Row count for sequencer 1");
			Assert.AreEqual(16, player.Cells.Count(x => x.DisplayRow == 0 && x.Sound == Player.SoundType.Drum), "Bad Row count for row 0 Drum");

			// Correct number or sounds set up
			Assert.AreEqual(32, player.Cells.Count(x => x.Sound == Player.SoundType.Snare));
			Assert.AreEqual(32, player.Cells.Count(x => x.Sound == Player.SoundType.Drum));

			// Display cells have correct sequencer position
			var testCell = player.GetCellByDisplayPosition(0, 0);
			Assert.AreEqual(0, testCell.SequencerRow, "Sequencer row failed  for 0 0 ");
			Assert.AreEqual(0, testCell.SequencerColumn, "Sequencer column failed for 0 0 ");

			var testCell2 = player.GetCellByDisplayPosition(2, 0);
			Assert.AreEqual(1, testCell2.SequencerRow, "Sequencer row failed for 0 2 ");
			Assert.AreEqual(0, testCell2.SequencerColumn, "Sequencer column failed for 0 2 ");

			// Sequencer Cells have the right display position
			var LastCellFirstRow = player.GetCellBySequencerPosition(0, 31);
			var LastCellSecondRow = player.GetCellBySequencerPosition(1, 31);
			
			Assert.AreEqual(1, LastCellFirstRow.DisplayRow);
			Assert.AreEqual(15, LastCellFirstRow.DisplayColumn);

			Assert.AreEqual(3, LastCellSecondRow.DisplayRow);
			Assert.AreEqual(15, LastCellSecondRow.DisplayColumn);

		}

		[Test]
		public void BeatTest()
		{
			var player = new Player(60, 60);
			player.Beat();
			player.Beat();

			Assert.AreEqual(2, player.CurrentBeat);

			for(int i = 0; i < 15; i++)
			{
				player.Beat();
			}

			Assert.AreEqual(0, player.CurrentBeat);
		}

		public void TurnBeatOn()
		{
			var player = new Player(60, 60);

		}

		[Test]
		public void RowPosition()
		{
			var player = new Player(60, 60);

			var cell = player.Cells.First(x => x.SequencerRow == 1 && x.SequencerColumn == 1);
			Assert.AreEqual(60, cell.GetRectangle().Top);
			Assert.AreEqual(60, cell.GetRectangle().Left);

		}
	}
}
